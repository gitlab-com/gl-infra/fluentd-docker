# fluentd-docker

This is a fork of
https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/fluentd-elasticsearch/fluentd-es-image,
with added features required by GitLab.

This directory contains the source files needed to make a Docker image
that collects Docker container log files using [Fluentd][fluentd]
and sends them to an instance of [Elasticsearch][elasticsearch].
This image is designed to be used as part of the [Kubernetes][kubernetes]
cluster bring up process.

[fluentd]: http://www.fluentd.org/
[elasticsearch]: https://www.elastic.co/products/elasticsearch
[kubernetes]: https://kubernetes.io

## Usage

GitLab CI automatically builds and pushes Docker images to this project's image
registry, for git tags only. The Docker image tag is named after the git tag.

These images are intended to be referenced in
https://gitlab.com/gitlab-org/charts/fluentd-elasticsearch.

We release from the master branch by running:

```
git tag vX.Y.Z
git push --tags
```

### Changing Ruby dependencies

Edit the `Gemfile`, and run `./docker-bundle.sh install` or `./docker-bundle.sh
update`, which will modify the checked-in `Gemfile.lock` if necessary. Make a
merge request.
