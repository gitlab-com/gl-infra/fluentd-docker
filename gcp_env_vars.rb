#!/usr/bin/env ruby

require 'uri'
require 'net/http'

http = Net::HTTP.new("metadata.google.internal", 80)
http.max_retries = 3
req = Net::HTTP::Get.new("/computeMetadata/v1/instance/zone")
req['Metadata-Flavor'] = 'Google'

begin
  res = http.request(req)
rescue StandardError
  exit(0)
end

if res.is_a?(Net::HTTPSuccess)
  zone_raw = res.body
  zone = zone_raw.split('/').last
  region = zone.split('-').reverse.drop(1).reverse.join('-')
  puts "export GCP_ZONE=#{zone}"
  puts "export GCP_REGION=#{region}"
end
